import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CallbackComponent } from './callback.component';

import { HttpClientModule } from '@angular/common/http';

import { WSService } from './ws.service';

import { AuthService } from './auth/auth.service';
import { AuthenticationComponent } from './authentication/authentication.component';

import { FormsModule } from '@angular/forms';
import { ChatComponent } from './chat/chat.component';

import {SocketService} from './socket.service';

import { Uploader }      from './local_modules/angular2-http-upload';

import {Config} from './config';


import {NgxAutoScroll} from "ngx-auto-scroll/lib/ngx-auto-scroll.directive";

import { PipeTransform, Pipe } from '@angular/core';
@Pipe({name: 'values'})
export class KeysPipe implements PipeTransform {
  transform(value, args:string[]) : any {
    return Object.values(value);
    // let keys = [];
    // for (let key in value) {
    //   keys.push({key: key, value: value[key]});
    // }
    // return keys;
  }
}

@NgModule({
  declarations: [
    AppComponent,
    CallbackComponent,
    AuthenticationComponent,
    ChatComponent,
    KeysPipe,
    NgxAutoScroll
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [WSService, AuthService, SocketService, Uploader, Config],
  bootstrap: [AppComponent]
})
export class AppModule { }
