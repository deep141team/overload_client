import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
describe('AppComponent', () => {
  
  it(`should have as title 'app'`, async(() => {
    expect('app').toEqual('app');
  }));
});
