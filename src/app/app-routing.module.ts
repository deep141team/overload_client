import { NgModule } from '@angular/core';
import { AuthenticationComponent } from './authentication/authentication.component';
import { ChatComponent } from './chat/chat.component';
import { Routes, RouterModule, CanActivate } from '@angular/router';

import { AuthGuard } from './auth/auth.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'chat',
    pathMatch: 'full'
  },
  {
    path: 'authentication',
    component: AuthenticationComponent,
  },
  {
    path: 'chat',
    component: ChatComponent,
    // Add this to guard this route
    canActivate: [
      AuthGuard
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [AuthGuard]
})
export class AppRoutingModule { }