import { Component, OnInit } from '@angular/core';


import { SocketService } from '../socket.service';

import { AuthService } from '../auth/auth.service';

import { MyUploadItem }  from '../upload.item';

import { Uploader }      from '../local_modules/angular2-http-upload';

import {Config} from '../config';

import {DatePipe} from '@angular/common';

import { Router } from '@angular/router';

import { tokenNotExpired } from 'angular2-jwt';


@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {

	private message = {
		room: '',
		from: '',
		text: '',
		target: ''
	};
	/*
		room_id: [
			message
		]
	*/
	public rooms = {
	};
	public upload_rooms = {
	}

	public groups = {

	};

	public users = [];

	// user id
	public selected_room = 'placeholder';

	private connections;

	private user;

	public show_prepare_group = false;

	public new_group = {
		title: '',
		users: [],
		error: ''
	};

	private config: Config;
	private window: any;

	public chatHeight;

	private expireInterval: any;

	private video_format = ['mp4', 'ogg', 'webm', 'ogv'];
	private video_type={'mp4': 'video/mp4', 'webm': 'video/webm', ogg: 'video/ogg', ogv: 'video/ogg'};

	constructor(private socketService: SocketService, 
		private authService: AuthService,
		public uploaderService: Uploader,
		private router: Router
		) {
		this.window = window;

		this.chatHeight = this.window.innerHeight - 280;	
		this.config = new Config();
		this.user = this.authService.getUser();
		console.log('user', this.user)
		this.socketService.users.login(this.user);

		this.expireInterval = setInterval(() => {
	      if (!tokenNotExpired('token')) {
	        clearInterval(this.expireInterval);
	        this.router.navigate(['authentication']);
	      }
	    }, 500);
	}

	onResize(event) {
	  this.chatHeight = this.window.innerHeight - 280;
	}

	private usersInGroup(id) {
		return id.split(':').length;
	}
	public isRead(m) {
		return this.usersInGroup(this.selected_room) === m.read_by.length
			&& m.from == this.user.id;
	}

	public isDelivered(m) {
		return this.usersInGroup(this.selected_room) === m.delivered_to.length
			&& m.from == this.user.id;
	}

	public getImage(file, user_id) {
		if (user_id) {
			if (this.user.id == user_id) {
				file = this.user['photo']
			} else {
				let user = this.users.filter((el) => el.id == user_id)[0]
				if (user) {
					file = user['photo'];
				}
			}
		}
		if (!file) {
			return '../../assets/profile.png';
		}
		let url = this.config.assets_base_url;
		url = url.replace('file_name', file);
		url = url.replace('token_here', localStorage.getItem('token'));
		return url;
	}

	private timeout;
	private finalDoneTypingInterval = 500;
	private sentEvent = false; 

	// room_id: {sent: []. pending:[]}
	private read_messages = {

	};
	public timeSince(date) {

		date = new Date(date);

	  var seconds = Math.floor(((new Date()).getTime() - date) / 1000);

	  var interval = Math.floor(seconds / 86400);

	  if (interval > 2) {
	  	 var datePipe = new DatePipe("en-US");
        return datePipe.transform(date, 'MM-dd-yyyy HH:MM a');
	  }
	  if (interval >= 1 ) {
	    return interval + (interval == 1 ? ' day' : ' days') + " ago";
	  }
	  interval = Math.floor(seconds / 3600);
	  if (interval >= 1) {
	    return interval + (interval == 1 ? ' hour' : ' hours') + " ago";
	  }
	  interval = Math.floor(seconds / 60);
	  if (interval >= 1) {
	    return interval + (interval == 1 ? ' minute' : ' minutes') + " ago";
	  }
	  return "now";
	  // return Math.floor(seconds) + " seconds";
	}
	sendStartTyping() {
		this.socketService.rooms.startTyping(this.selected_room);
	}
	sendStopTyping() {
		this.socketService.rooms.stopTyping(this.selected_room);
	}

	startTyping(event) {

		if (event.keyCode == 13) {
			this.sendStopTyping();
			this.sendMessage();
		}
		
		clearTimeout(this.timeout);
				
		if (this.message.text != '' && !this.sentEvent) {
			this.sendStartTyping();
			this.sentEvent = true;
		}

		this.timeout = setTimeout(() => {
			this.sendStopTyping();
			this.sentEvent = false;
		}, this.finalDoneTypingInterval);

		
	}

	submit(el_id) {
        let uploadFiles = (<HTMLInputElement>window.document.getElementById(el_id)).files;

        this.uploaderService.onSuccessUpload = (item, response, status, headers) => {
             // success callback
			delete this.upload_rooms[this.selected_room][item.file.name];
         	this.upload_rooms = JSON.parse(JSON.stringify(this.upload_rooms));
        };
        this.uploaderService.onErrorUpload = (item, response, status, headers) => {
             // error callback
             this.upload_rooms[this.selected_room][item.file.name].text = "File not sent: " + item.file.name;
        };

        this.uploaderService.onProgressUpload = (item, percentComplete) => {
        	if(this.upload_rooms[this.selected_room][item.file.name]) {
	        	this.upload_rooms[this.selected_room][item.file.name].progress = percentComplete;
	        	this.upload_rooms = JSON.parse(JSON.stringify(this.upload_rooms));
        	}
             // progress callback
        };
        for (var i = uploadFiles.length - 1; i >= 0; i--) {
 			let uploadFile = uploadFiles[i];

 			
	        let myUploadItem = new MyUploadItem(uploadFile, {
	        	user_id: this.user.id,
	        	room_id: this.selected_room
	        });
	        myUploadItem.formData = { file: uploadFile };  // (optional) form data can be sent with file
	 		
	 		if (!this.upload_rooms[this.selected_room]) {
	        	this.upload_rooms[this.selected_room] = {};
	 		}

	        this.upload_rooms[this.selected_room][uploadFile.name] = {
	        	room: this.selected_room,
				from: this.user.id,
				text: "uploading file: " + uploadFile.name,
				progress: 0,
				state: 'progress',
				target: ''
	        };

	        this.uploaderService.upload(myUploadItem)

	        this.upload_rooms = JSON.parse(JSON.stringify(this.upload_rooms));
 		}
    }

	showChat() {
		return this.message.from !== '';
	}

	getShowPrepareGroup() {
		return this.show_prepare_group === true ? 'show' : 'hide';
	}

	hidePrepareGroup() {
		this.show_prepare_group = false;
	}

	prepareCreateGroup() {
		this.show_prepare_group = true;
		this.new_group = {
			title: '',
			users: [this.user.id],
			error: ''
		};
	}
	removeNewGroupError() {
		this.new_group.error = '';
	}

	createGroup() {

		if (this.new_group.title == '') {
			this.new_group.error = 'Group name is required!';
			return;
		}

		if (this.new_group.users.length < 3) {
			this.new_group.error = 'Please select at least 2 users!';
			return;
		}

		
		let room_id = this.new_group.users.sort().join(':');

		if (this.rooms[room_id]) {
			this.new_group.error = 'There is already a group with these users!';
			return;
		}

		this.show_prepare_group = false;

		this.message.room = room_id;
		this.rooms[room_id] = {
			messages: [],
			room_id: room_id,
			room_title: this.new_group.title
		}

		this.socketService.rooms.getMessages({
			connections: room_id.split(':'),
			room: room_id,
			room_title: this.new_group.title
		});

		this.rooms = JSON.parse(JSON.stringify(this.rooms));

		this.socketService.rooms.create({
			id: room_id,
			title: this.new_group.title
		})
	}
	

	addUserToNewGroup(user_id) {
		
		if (this.new_group.users.indexOf(user_id) > -1) {
			this.new_group.users = this.new_group.users.filter(function(el) {
				return el !== user_id
			});		
		} else {
			this.new_group.users.push(user_id);
		}

	}
	getNewGroupUser(user_id) {
		return this.new_group.users.indexOf(user_id) > -1 ? 'selected' : '';
	}
	getDirection(m) {
		return m.from == this.user.id ? 'left' : 'right'
	}

	getActiveRoom(room_id) {
		return this.selected_room === room_id ? 'active' : ''
	}

	sendMessage() {
		if (this.message.text == '') {
			return;
		}
		this.socketService.rooms.sendMessage(this.message);
		this.message.text = '';
	}
	renderVideo(path) {
		return this.video_format.indexOf(path.split('.').pop()) > -1;
	}
	videoFormat(path) {
		return this.video_type[path.split('.').pop()];
	}
	// target: user|group
	selectRoom(room_id) {

		this.upload_rooms[room_id] = {
			messages: []
		};

		if (!this.rooms[room_id]) {
			this.rooms[room_id] = {
				messages: []
			};
		}
		
		this.message = {
			room: room_id,
			from: this.user.id,
			text: '',
			target: 'user'
		}

		this.socketService.rooms.getMessages({
			connections: room_id.split(':'),
			room: room_id
		});

		this.selected_room = room_id;
	}
	getRoomId(arr) {
		return arr.sort().join(':');
	}
	isOwner(owner) {
		return owner == this.user.id;
	}

	deleteRoom(id) {
		this.socketService.rooms.deleteRoom(id);
	}


	show_add_user_in_room = false;

	edit_room;
	

	showAddUserInRoom() {
		return this.show_add_user_in_room === true ? 'show' : 'hide';
	}

	hideAddUserInRoom() {
		this.show_add_user_in_room = false;
	}
	editRoomAdd(user_id) {
		if (this.edit_room.users.indexOf(user_id) > -1) {
			this.edit_room.users = this.edit_room.users.filter(function(el) {
				return el !== user_id
			});		
		} else {
			this.edit_room.users.push(user_id);
		}
	}
	selectedUserEditRoom(user_id) {
		console.log(this.edit_room.users, user_id);
		return this.edit_room.users.indexOf(user_id) > -1 ? 'selected' : '';
	}

	prepareAddUserInRoom(id) {
		this.show_add_user_in_room = true;

		this.edit_room = this.rooms[id];
		this.edit_room.users = this.edit_room.room_id.split(':');
	}


	editGroup() {

		if (this.edit_room.users.length < 3) {
			this.edit_room.error = 'Please select at least 2 users!';
			return;
		}

		
		let room_id = this.edit_room.users.sort().join(':');

		if (this.rooms[room_id]) {
			this.new_group.error = 'There is already a group with these users!';
			return;
		}

		this.show_add_user_in_room = false;


		delete this.rooms[this.edit_room.room_id];


		this.rooms = JSON.parse(JSON.stringify(this.rooms));

		this.socketService.rooms.editRoom({
			room_id: room_id,
			old_room_id: this.edit_room.room_id
		});
	}
	// addUsersInRoom(id) {
	// 	console.log('add users',id);

	// }


	getUserWithId(id) {

		let user = this.users.filter((el) => {
			return el.id === id
		});

		return user[0] ? user[0] : this.user;
	}

	isLast(element, array) {
		return array.indexOf(element) < array.length -1;
	}
	unreadMessages(room_id) {
		let count = 0;
		if (this.rooms[room_id]) {
			count = this.rooms[room_id].messages.filter((m) => m.read_by.indexOf(this.user.id) == -1).length;
		}
		return count > 0 ? count : false;
	}

	readMessage(message_ids, room_id) {
		if (!this.read_messages[room_id]) {
			this.read_messages[room_id] = {
				sent: [],
				pending: []
			}
		}

		this.read_messages[room_id].sent.forEach((el) => {
			if (this.read_messages[room_id].pending.indexOf(el) == -1) {
				this.read_messages[room_id].pending.push(el);
			}
		})

		message_ids = message_ids.filter((el) => this.read_messages[room_id].pending.indexOf(el) == -1);

		this.read_messages[room_id].sent = message_ids;

		if (message_ids.length > 0) {
			this.socketService.rooms.readMessage({
				room_id: room_id,
				messages: message_ids
			});
		}
	}

	ngOnInit() {

		this.connections = [
			this.socketService.users.getList().subscribe(users => {
				this.users = users.filter((u) => {
					return u.id != this.user.id;
				});
				this.users = JSON.parse(JSON.stringify(this.users));
			}),
			this.socketService.users.updateUser().subscribe(update => {

				let user_index = this.users.findIndex(u => u.id == update.user_id)

				if (user_index > -1) {
					Object.keys(update.update).forEach((key) => {
						this.users[user_index][key] = update.update[key];	
					});
				} else {
					this.users.push(update.update);
				}

			}),

			this.socketService.rooms.getList().subscribe(rooms => {
				this.rooms = {};
				Object.values(rooms).forEach((room) => {
					this.rooms[room.room_id] = room;
					this.rooms[room.room_id].room_title = room.title;
				});

				this.rooms = JSON.parse(JSON.stringify(this.rooms));
				if (!this.rooms[this.selected_room]) {
					this.message.from = '';
					this.selected_room = 'placeholder';
				}
			}),

			// response : {room_id, messages}
			this.socketService.rooms.listMessages().subscribe(response => {
				this.rooms[response.room_id] = {
					messages: response.messages,
					room_id: response.room_id,
					title: response.room_title
				}

				if (response.room_id == this.selected_room) {
					let message_ids = response.messages.reduce((carry, m) => {
						if (m.read_by.indexOf(this.user.id) == -1) {
							carry.push(m.id);
						}
						return carry;
					}, []);	

					this.readMessage(message_ids, response.room_id);
				}
			}),

			this.socketService.rooms.getMessage().subscribe(message => {
				if (!this.rooms[message.room_id]) {
					this.rooms[message.room_id] = {};
					this.rooms[message.room_id].messages = [];
				}

				this.rooms[message.room_id].messages.push(message.message);

				this.rooms[message.room_id] = JSON.parse(JSON.stringify(this.rooms[message.room_id]));

				if (message.room_id == this.selected_room) {
					this.readMessage([message.message.id], message.room_id);
				}
			}),

			// message: room_id, author
			this.socketService.rooms.getStartTyping().subscribe(message => {
				if (!this.rooms[message.room_id]) return;
				if (!this.rooms[message.room_id].typing) {
					this.rooms[message.room_id].typing = [];
				}
				this.rooms[message.room_id].typing.push(message.author);
				this.rooms[message.room_id] = JSON.parse(JSON.stringify(this.rooms[message.room_id]));
			}),

			// message: room_id, author
			this.socketService.rooms.getStopTyping().subscribe(message => {
				if (!this.rooms[message.room_id] || !this.rooms[message.room_id].typing) {
					return;
				}

				this.rooms[message.room_id].typing = this.rooms[message.room_id].typing.filter((el) => el !== message.author);
				this.rooms[message.room_id] = JSON.parse(JSON.stringify(this.rooms[message.room_id]));
			}),

			this.socketService.rooms.deletedRoom().subscribe(message => {
				if (!this.rooms[message.room_id]) return;

				delete this.rooms[message.room_id];
				this.message.from = '';


				if (this.selected_room == message.room_id) {
					this.selected_room = 'placeholder';
				}


				this.rooms = JSON.parse(JSON.stringify(this.rooms));
			})
			
		];
	}

	ngOnDestroy() {
		for (var i = this.connections.length - 1; i >= 0; i--) {
			this.connections[i].unsubscribe()
		}
	}

}
