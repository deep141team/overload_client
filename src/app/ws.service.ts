import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { catchError } from 'rxjs/operators';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

import {Config} from "./config";

@Injectable()
export class WSService {
  private apiUrl;
  private webUrl;

  constructor(private http: HttpClient, private config: Config) { 
    this.config = new Config();
    this.apiUrl = this.config.api_base_url;
    this.webUrl = this.config.server_base_url;
  }

  // Implement a method to handle errors if any
  private handleError(err: HttpErrorResponse | any) {
    console.error('An error occurred', err);
    return Observable.throw(err.message || err);
  }

  public login(data) {
    return this.http
      .post(this.webUrl + 'login', data, {
        headers: new HttpHeaders().set('Content-Type', `application/json`)
      })
      .pipe(
        catchError(this.handleError)
      );
  }

  public register(data,cb) {
    let xhr  = new (<any>window).XMLHttpRequest();
    let form = new (<any>window).FormData();

    let fileList = data.photo;
    if (fileList) {
      
    let file: File = fileList[0];
    if(fileList.length > 0) {
      form.append('file', file, file.name);
    }
    
    }
    



    form.append('username', data.username);
    form.append('email', data.email);
    form.append('password', data.password);
    form.append('confirm_password', data.confirm_password);

    xhr.onload = () => {
        let headers = this.parseHeaders(xhr.getAllResponseHeaders());
        let response = this.parseResponse(headers['Content-Type'], xhr.response);
        cb(response)
    };

    xhr.open('POST', this.webUrl + 'register', true);
    
    xhr.send(form);
  }

  private parseHeaders(headers: string) {
        let dict = {};
        let lines = headers.split('\n');
        for (let i = 0; i < lines.length; i++) {
            let entry = lines[i].split(': ');
            if(entry.length > 1) {
                dict[entry[0]] = entry[1];
            }
        }

        return dict;
    }

    private parseResponse(contentType: string, response: string) {
        let parsed = response;
        if(contentType && contentType.indexOf('application/json') === 0) {
            try {
                parsed = JSON.parse(response);
            } catch(e) {
            }
        }
        return parsed;
    }
}