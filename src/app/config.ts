
var base_url = 'http://localhost:3000';

export class Config {
  sockets_base_url = base_url;
  api_base_url = base_url + '/api/';
  server_base_url = base_url + '/';
  assets_base_url = base_url + '/api/files/file_name?token=token_here';
}
