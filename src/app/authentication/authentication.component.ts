
import { Component, OnInit } from '@angular/core';

import { AuthService } from './../auth/auth.service';

import { WSService } from '../ws.service';

@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.css']
})
export class AuthenticationComponent implements OnInit {
  tab;

  username;
  password;
  confirm_password;
  email;
  photo;

  error;

  constructor(private authService: AuthService, private ws: WSService) { 
    this.tab = 'login';
    this.error = '';
  }

  ngOnInit() {
  }

  fileChange(e) {
    this.photo = e.target['files'];
  }

  onTabClick(clicked) {
  	this.tab = clicked;
  }

  login() {
    this.error = '';
    let self = this;

    this.authService.login({
      email: this.email,
      password: this.password
    }, (err) => {
      self.showError(err.message);
    });
  }

  register() {
    this.error = '';
    let self = this;

    this.authService.register({
      email: this.email,
      username: this.username,
      photo: this.photo,
      password: this.password,
      confirm_password: this.confirm_password,
    }, (err) => {
      self.showError(err.message);
    }, (success) => {
      this.tab = 'login';
    });
  }

  showError(message) {
    this.error = message;
  }

}
