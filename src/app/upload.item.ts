import { UploadItem }    from './local_modules/angular2-http-upload';
 

import { HttpHeaders } from '@angular/common/http';

import {Config} from './config';

export class MyUploadItem extends UploadItem {
	config: Config;
    constructor(file: any, user: any) {
        super();

        console.log('file.user',user);
        this.config = new Config();
        this.url = this.config.api_base_url + 'upload?token=' + localStorage.getItem('token');
        this.body = user;
        this.file = file;
    }
}