import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import * as auth0 from 'auth0-js';
import { AUTH_CONFIG } from './auth-config';
import { Router } from '@angular/router';

import { WSService } from '../ws.service';

import { SocketService } from '../socket.service';

import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

  userProfile: any;

  // Create a stream of logged in status to communicate throughout app
  loggedIn: boolean;
  loggedIn$ = new BehaviorSubject<boolean>(this.loggedIn);

  constructor(private router: Router, private ws: WSService, public socket: SocketService) {
    // If authenticated, set local profile property and update login status subject
    // If token is expired, log out to clear any data from localStorage
    if (this.authenticated) {
      this.userProfile = this._getUserProfile();
      this.setLoggedIn(true);
    } else {
      this.logout();
    }
  }

  getUser() {
    return this._getUserProfile();
  }

  setLoggedIn(value: boolean) {
    // Update login status subject
    this.loggedIn$.next(value);
    this.loggedIn = value;
  }

  /*
    data: {email, password}
    res:  success: true,
          message: 'Enjoy your token!',
          token: token,
          user: {
            username: user.username,
            email: user.email
          }
  */
  login(data, callback) {
    return this.ws.login(data)
    .subscribe(
      data => {
        if (!data.success) {
          return callback(data);
        }

        this._setSession(data);
        this.router.navigate(['/']);
        this.socket.connect();
      },
      err => {
        callback(err);
      }
    );
  }

  /*
    data: {email, password}
    res:  success: true,
          message: 'Enjoy your token!',
          token: token,
          user: {
            username: user.username,
            email: user.email
          }
  */
  register(data, callback, cb2) {
    return this.ws.register(data, (data) => {
      data = JSON.parse(data);

      if (!data.success) {
          return callback(data);
        }
        cb2(data);
    });
  }

  handleAuth() {
    if (this.authenticated) {
      this.userProfile = this._getUserProfile();
      this.setLoggedIn(true);
    } else {
      this.router.navigate(['/']);
    }
  }

  private _getUserProfile() {
    return {
      username: localStorage.getItem('user_username'),
      email: localStorage.getItem('user_email'),
      id: localStorage.getItem('user_id'),
      token: localStorage.getItem('token'),
      user_id: localStorage.getItem('user_id'),
      photo: localStorage.getItem('photo')
    };
  }

  private _setSession(authResult) {
    const expTime = authResult.expiresIn * 1000 + Date.now();
    // Save session data and update login status subject
    localStorage.setItem('token', authResult.token);
    localStorage.setItem('user_username', authResult.user.username);
    localStorage.setItem('user_email', authResult.user.email);
    localStorage.setItem('user_id', authResult.user.id);
    localStorage.setItem('photo', authResult.user.photo);
    this.userProfile = this._getUserProfile();
    this.setLoggedIn(true);
  }

  logout() {
    // Remove tokens and profile and update login status subject
    localStorage.removeItem('token');
    localStorage.removeItem('user_username');
    localStorage.removeItem('user_email');
    localStorage.removeItem('photo');
    this.userProfile = undefined;
    this.setLoggedIn(false);
    this.router.navigate(['authentication']);
    this.socket.disconnect();
  }

  get authenticated(): boolean {
    // Check if current date is greater than expiration
    // const expiresAt = JSON.parse(localStorage.getItem('expires_at'));
    // return Date.now() < expiresAt;
    return tokenNotExpired("token");
  }

}