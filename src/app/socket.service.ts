import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

import * as io from "socket.io-client";

import {Config} from "./config";


class Base {

  public socket;

  constructor(socket) {
    this.socket = socket;
  }
}

class Users extends Base {

  login(data) {
    this.socket.emit('login', data);
  }

  getList() {
    return new Observable(observer => {
      this.socket.on('GET-LIST:users', (data) => {
        observer.next(data);    
      });
    })
  }

  updateUser() {
    return new Observable(observer => {
      this.socket.on('UPDATE:user', (data) => {
        observer.next(data);    
      });
    })
  }
  
}

class Rooms extends Base {
  getList() {
    return new Observable(observer => {
      this.socket.on('GET-LIST:groups', (data) => {
        observer.next(data);    
      });
    })
  }
  getMessages(data) {
    this.socket.emit('GET-LIST:group:messages', data); 
  }
  readMessage(data) {
    this.socket.emit('GET-LIST:group:messages:read', data); 
  }
  deleteRoom(data) {
    this.socket.emit('GET-LIST:group:delete', data);  
  }
  startTyping(room_id) {
   this.socket.emit('GET-LIST:group:typing:start', room_id);  
  }
  stopTyping(room_id) {
   this.socket.emit('GET-LIST:group:typing:stop', room_id);  
  }
  sendMessage(data) {
    this.socket.emit('NEW:group:message', data); 
  }
  deletedRoom() {
    return new Observable(observer => {
      this.socket.on('GET-LIST:group:delete', (data) => {
        observer.next(data);    
      });
    })
  }
  editRoom(data) {
    this.socket.emit('GET-LIST:group:edit', data); 
  }
  getMessage() {
    return new Observable(observer => {
      this.socket.on('NEW:group:message', (data) => {
        observer.next(data);    
      });
    })
  }
  getStartTyping() {
    return new Observable(observer => {
      this.socket.on('GET-LIST:group:typing:start', (data) => {
        observer.next(data);    
      });
    })
  }
  getStopTyping() {
    return new Observable(observer => {
      this.socket.on('GET-LIST:group:typing:stop', (data) => {
        observer.next(data);
      });
    })
  }
  listMessages() {
    return new Observable(observer => {
      this.socket.on('GET-LIST:group:messages', (data) => {
        observer.next(data);    
      });
    })
  }
  create(data) {
    this.socket.emit('NEW:group', data);
  }

}

class Chats extends Base {
  
}

@Injectable()
export class SocketService {
  private socketUrl;
  private socket;
  public users;
  public rooms;
  public chats;


  constructor(private config: Config) {
    this.socketUrl = this.config.sockets_base_url;

    this.connect();
  }

  connect() {
    let socket = io(this.socketUrl + '/users');

    this.users = new Users(socket);
    this.rooms = new Rooms(socket);
    this.chats = new Chats(socket);
    this.socket = socket;
  }

  disconnect() {
    return this.socket.disconnect();
  }
}